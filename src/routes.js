export default [
  {
    path: '/geo',
    component: require('./assets/vue/pages/index.vue')
  },
    {
    path: '/geo/bus/:plateNumber',
    component: require('./assets/vue/pages/selected-bus.vue')
  }
]
